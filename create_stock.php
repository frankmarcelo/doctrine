<?php
/**
 * File: create_stock.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  create_stock.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 */

require_once "bootstrap.php";

$market = new Market("Some Exchange");
$stock1 = new Stock("AAPL", $market);
$stock2 = new Stock("GOOG", $market);

$entityManager->persist($market);
$entityManager->persist($stock1);
$entityManager->persist($stock2);
$entityManager->flush();

echo "Created Product with ID " . $market->getId() . "\n";