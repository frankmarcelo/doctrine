<?php
/**
 * File: create_user.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  create_user.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 */
require_once "bootstrap.php";

$newUsername = $argv[1];

$user = new User();
$user->setName($newUsername);

$entityManager->persist($user);
$entityManager->flush();

echo "Created User with ID " . $user->getId() . "\n";