<?php
/**
 * File: show_bug.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  show_bug.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 */

require_once "bootstrap.php";

$theBugId = $argv[1];

$bug = $entityManager->find("Bug", (int)$theBugId);

echo "Bug: ".$bug->getDescription()."\n";
echo "Engineer: ".$bug->getEngineer()->getName()."\n";