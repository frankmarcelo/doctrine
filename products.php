<?php
/**
 * File: products.php
 *
 * PHP version 5.4
 *
 * @category Bootstrap
 * @package  products.php
 * @author   Franklin Marcelo <fmarcelo@csod.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://csb.csod.com/learning
 */
require_once "bootstrap.php";

$dql = "SELECT p.id, p.name, count(b.id) AS openBugs FROM Bug b ".
    "JOIN b.products p WHERE b.status = 'OPEN' GROUP BY p.id";
$productBugs = $entityManager->createQuery($dql)->getScalarResult();

foreach ($productBugs as $productBug) {
    echo $productBug['name']." has " . $productBug['openBugs'] . " open bugs!\n";
}